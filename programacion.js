
  var x,y,z;

  function calcular(){
  
      // Variables 
  
    
      var resultado= document.getElementById("resultado");
      var suma= document.getElementById("suma");
      var resta= document.getElementById("resta");
      var division= document.getElementById("division");
      var multiplicacion= document.getElementById("multiplicacion");
      var reset= document.getElementById("reset");
      var uno= document.getElementById("uno");
      var dos= document.getElementById("dos");
      var tres= document.getElementById("tres");
      var cuatro= document.getElementById("cuatro");
      var cinco= document.getElementById("cinco");
      var seis= document.getElementById("seis");
      var siete= document.getElementById("siete");
      var ocho= document.getElementById("ocho");
      var nueve= document.getElementById("nueve");
      var cero= document.getElementById("cero");
      var igual= document.getElementById("igual");
  
      //Procesos
  
  uno.onclick= function(e){
          resultado.textContent = resultado.textContent + "1";   
      }
  
  dos.onclick= function(e){
          resultado.textContent= resultado.textContent + "2";
  }
  
  tres.onclick= function(e){
      resultado.textContent= resultado.textContent + "3";
  }
  
  cuatro.onclick= function(e){
      resultado.textContent= resultado.textContent + "4";
  }
  
  cinco.onclick= function(e){
      resultado.textContent= resultado.textContent + "5";
  }
  
  seis.onclick= function(e){
      resultado.textContent= resultado.textContent+"6";
  }
  
  siete.onclick= function(e){
      resultado.textContent= resultado.textContent+"7";
  }
  
  ocho.onclick= function(e){
      resultado.textContent= resultado.textContent+"8";
  }
  
  nueve.onclick= function(e){
      resultado.textContent= resultado.textContent+"9";
  }
  
  cero.onclick= function(e){
      resultado.textContent= resultado.textContent+"0";
  }
  
  
  reset.onclick= function(e){
      Resetear();
  }
  
  
  suma.onclick = function(e){
  
      x=resultado.textContent;
      z="+";
      limpiar();
  }
  
  resta.onclick = function(e){
  
      x=resultado.textContent;
      z="-";
      limpiar();
  }
  
  
  division.onclick = function(e){
  
      x=resultado.textContent;
      z="/";
      limpiar();
  }
  
  
  multiplicacion.onclick = function(e){
  
      x=resultado.textContent;
      z="*";
      limpiar();
  }
  
  
  igual.onclick = function(e){
  
      y= resultado.textContent;
      resolver();
  }
  
  }
  
  
  
  function limpiar(){
      
      resultado.textContent = "";
  }
  
  function Resetear(){
  
      resultado.textContent= "";
      x=0;
      y=0;
      z="";
  
  }
  
  
  function resolver(){
      let i = 0;
  
      switch(z){
  
          case "+":
              i = parseFloat(x) + parseFloat (y);
              break;
  
              case "-":
              i = parseFloat(x) - parseFloat (y);
              break;
  
              case "*":
              i = parseFloat(x) * parseFloat (y);
              break;
  
              case "/":
              i = parseFloat(x) / parseFloat (y);
              break;
      }
       
      Resetear();
      resultado.textContent = i;
  
  }
  